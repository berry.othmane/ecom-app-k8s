import requests

# Constants
GITLAB_TOKEN = "glpat-tzb1DW4tm3xtzFBraywk"
PROJECT_ID = "57189209"
REPO_NAME = "ecom-frontend-image"
DEPLOYMENT_FILE_PATH = "C:\\Users\\Othmane Berrouyne\\Desktop\\k8s-manifest\\frontend-deployment.yaml"

def get_registry_id(token, project_id, repo_name):
    headers = {'Private-Token': token}
    url = f'https://gitlab.com/api/v4/projects/{project_id}/registry/repositories'
    response = requests.get(url, headers=headers)
    if response.status_code == 200:
        for repo in response.json():
            if repo['name'] == repo_name:
                return repo['id']
        return None  # Registry ID not found
    else:
        print(f"Failed to fetch registry repositories: {response.status_code} {response.reason}")
        return None

def get_image_tags(token, project_id, registry_id):
    headers = {'Private-Token': token}
    url = f'https://gitlab.com/api/v4/projects/{project_id}/registry/repositories/{registry_id}/tags'
    
    response = requests.get(url, headers=headers)
    if response.status_code == 200:
        tags = [tag['name'] for tag in response.json()]
        if tags:
            tags.sort()  # Sort the tags, assuming alphabetical order reflects newer versions
            return tags[-1]  # Return the latest tag
        return None  # No tags found
    else:
        return f"Failed to fetch tags: {response.status_code} {response.reason}"

# Usage
registry_id = get_registry_id(GITLAB_TOKEN, PROJECT_ID, REPO_NAME)
if registry_id:
    latest_tag = get_image_tags(GITLAB_TOKEN, PROJECT_ID, registry_id)
    if latest_tag:
        print( latest_tag)
    else:
        print("No tags found for this repository.")
else:
    print(f"No registry ID found for repository {REPO_NAME}")

#!/bin/bash

set -e

# Variables
FRONTEND_REPO="registry.gitlab.com/berry.othmane/ecom-frontend-app/ecom-frontend-image"
BACKEND_REPO="registry.gitlab.com/berry.othmane/ecom-backend-app/ecom-backend-image"
FRONTEND_MANIFEST_PATH="kubernetes-manifests/frontend-deployment.yaml"
BACKEND_MANIFEST_PATH="kubernetes-manifests/backend-deployment.yaml"

# Fetch the latest tags from the frontend repository
FRONTEND_TAG=$(curl -s --header "PRIVATE-TOKEN: $PERSONAL_ACCESS_TOKEN" "https://registry.gitlab.com/v2/${FRONTEND_REPO}/tags/list" | jq -r '.tags | sort | last')

# Fetch the latest tags from the backend repository
BACKEND_TAG=$(curl -s --header "PRIVATE-TOKEN: $PERSONAL_ACCESS_TOKEN" "https://registry.gitlab.com/v2/${BACKEND_REPO}/tags/list" | jq -r '.tags | sort | last')

# Update the frontend manifest
sed -i "s|image: registry.gitlab.com/${FRONTEND_REPO}:.*|image: registry.gitlab.com/${FRONTEND_REPO}:${FRONTEND_TAG}|" ${FRONTEND_MANIFEST_PATH}

# Update the backend manifest
sed -i "s|image: registry.gitlab.com/${BACKEND_REPO}:.*|image: registry.gitlab.com/${BACKEND_REPO}:${BACKEND_TAG}|" ${BACKEND_MANIFEST_PATH}

# Commit and push the changes
git config --global user.email "${GITLAB_USER_EMAIL}"
git config --global user.name "${GITLAB_USER_NAME}"
git add ${FRONTEND_MANIFEST_PATH} ${BACKEND_MANIFEST_PATH}
git commit -m "Update image tags to frontend:${FRONTEND_TAG} backend:${BACKEND_TAG}"
git push "https://${CI_JOB_TOKEN}@gitlab.com/${MANIFEST_REPO}.git" HEAD:
